package herramientas;

import abstracts.Calentador;

public class CalentadorSolar extends Calentador {

    StringBuffer salida = new StringBuffer();

    public CalentadorSolar(String modelo, String capacidad, int tubos){
        super(modelo,capacidad,tubos);
    }
    @Override
    public void mostrarCaracteristicas(){
        salida.append("el modelo es");
        salida.append(modelo);
        salida.append("la capacidad es");
        salida.append(capacidad);
        salida.append("los tubos son");
        salida.append(tubos);
        System.out.println(salida);

    }
}
